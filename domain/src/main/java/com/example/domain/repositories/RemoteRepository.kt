package com.example.domain.repositories

import com.example.domain.response.GetResponse
import com.example.utils.base.api.ApiResponse

interface RemoteRepository {
    suspend fun getApiResponse(): ApiResponse<GetResponse>
}