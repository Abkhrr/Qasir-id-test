package com.example.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ProductModel(
    @SerializedName("product_id")
    var id: Long,

    @SerializedName("product_name")
    var productName: String,

    @SerializedName("price")
    var price: Int,

    @SerializedName("stock")
    var stock: Int,

    @SerializedName("description")
    var description: String,

    @SerializedName("images")
    var images: ProductImagesModel
): Parcelable