package com.example.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ProductImagesModel(
    @SerializedName("thumbnail")
    var thumbnail: String,

    @SerializedName("large")
    var large: String
): Parcelable