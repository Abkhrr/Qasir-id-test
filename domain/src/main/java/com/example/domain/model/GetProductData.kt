package com.example.domain.model

import com.google.gson.annotations.SerializedName

data class GetProductData(
    @SerializedName("banner")
    val banner: String,

    @SerializedName("products")
    var products: List<ProductModel> = listOf()
)