package com.example.domain.response

import com.example.domain.model.GetProductData
import com.google.gson.annotations.SerializedName

data class GetResponse(
    @SerializedName("status")
    var status: String,

    @SerializedName("message")
    var message: String,

    @SerializedName("data")
    var data: GetProductData
)