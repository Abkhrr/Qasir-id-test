package com.example.utils.ext.api

enum class TypeError {
    ERROR_HTTP,
    ERROR_SOCKET_TIMEOUT,
    NETWORK_ERROR
}