package com.example.utils.ext.debug

import com.example.utils.BuildConfig

fun debug(action: () -> Unit) {
    if (BuildConfig.DEBUG) {
        action.invoke()
    }
}