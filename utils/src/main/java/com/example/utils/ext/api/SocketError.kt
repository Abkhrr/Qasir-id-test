package com.example.utils.ext.api

enum class SocketError(val code: Int) {
    SocketTimeOut(-1)
}