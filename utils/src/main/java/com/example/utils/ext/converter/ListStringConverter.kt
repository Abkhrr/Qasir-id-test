package com.example.utils.ext.converter

import androidx.room.TypeConverter
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import java.util.*
import kotlin.collections.ArrayList

class ListStringConverter {
    private var gson = Gson()

    @TypeConverter
    fun fromTimestamp(data: String?): List<String>? {

        if (data == null){
            return Collections.emptyList()
        }
        val listType = object : TypeToken<ArrayList<String>>() {}.type
        return gson.fromJson(data, listType)


    }

    @TypeConverter
    fun someObjectListToString(someObjects: List<String>?): String? {
        return gson.toJson(someObjects)
    }

}