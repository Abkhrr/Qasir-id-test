package com.example.utils.base.listener

interface BaseItemListener<T> {
    fun onItemClick(item: T)
    fun onRetryClick()
}