package com.example.utils.base.viewmodel

import androidx.lifecycle.ViewModel
import com.example.utils.ext.event.SingleLiveEvent

abstract class BaseViewModel: ViewModel() {
    val isLoading: SingleLiveEvent<Boolean> = SingleLiveEvent()
    val showToast: SingleLiveEvent<String>  = SingleLiveEvent()
    val showSnack: SingleLiveEvent<String>  = SingleLiveEvent()
}