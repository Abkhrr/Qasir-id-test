package com.example.ext.remote

import com.example.domain.response.GetResponse
import com.example.utils.base.api.ApiEndPoint
import retrofit2.http.POST

interface ApiService {
    @POST(ApiEndPoint.END_POINTS)
    suspend fun getResponse(): GetResponse
}