package com.example.ext.repositoryimpl

import com.example.domain.repositories.RemoteRepository
import com.example.domain.response.GetResponse
import com.example.ext.remote.ApiService
import com.example.utils.base.api.ApiResponse
import com.example.utils.ext.api.SocketError
import com.example.utils.ext.api.TypeError
import retrofit2.HttpException
import java.lang.Exception
import java.net.SocketTimeoutException
import javax.inject.Inject

class RemoteRepositoryImpl @Inject constructor(private val apiService: ApiService): RemoteRepository {

    override suspend fun getApiResponse(): ApiResponse<GetResponse> {
        return try {
            val getResponse = apiService.getResponse()
            ApiResponse.Success(getResponse)
        } catch (e: Exception){
            getExceptionType(e)
        }
    }

    private fun getExceptionType(e: Exception) : ApiResponse.Error {
        return when (e) {
            is HttpException -> ApiResponse.Error(e.message(), e.code(), TypeError.ERROR_HTTP)
            is SocketTimeoutException -> ApiResponse.Error(e.localizedMessage, SocketError.SocketTimeOut.code, TypeError.ERROR_SOCKET_TIMEOUT)
            else                      -> ApiResponse.Error(e.localizedMessage, Int.MAX_VALUE, TypeError.NETWORK_ERROR)
        }
    }

}